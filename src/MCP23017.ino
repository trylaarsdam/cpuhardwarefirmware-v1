#include "Particle.h"
using namespace std;

SYSTEM_THREAD(ENABLED);
std::string cpuVersion = "7/31/20 - 1500";
#include "MCP23017-RK.h"
#include "blynk.h"
#include <string>
#include "MQTT.h"
#include <vector>
#include <stdlib.h>
#define BANK_0 D2
#define BANK_1 D3
#define BANK_2 D4
#define BANK_3 D5
MQTT client("lab.thewcl.com", 1883, callback);
MCP23017 gpioData0(Wire, 0);
MCP23017 gpioData0_1(Wire, 0);
MCP23017 gpioData0_2(Wire, 0);
MCP23017 gpioData0_3(Wire, 0);

MCP23017 gpioData1(Wire, 2);
MCP23017 gpioData1_1(Wire, 2);
MCP23017 gpioData1_2(Wire, 2);
MCP23017 gpioData1_3(Wire, 2);

MCP23017 gpioData2(Wire, 4);
MCP23017 gpioData2_1(Wire, 4);
MCP23017 gpioData2_2(Wire, 4);
MCP23017 gpioData2_3(Wire, 4);

MCP23017 gpioData3(Wire, 6);
MCP23017 gpioData3_1(Wire, 6);
MCP23017 gpioData3_2(Wire, 6);
MCP23017 gpioData3_3(Wire, 6);

MCP23017 gpioControl0(Wire, 1);
MCP23017 gpioControl0_1(Wire, 1);
MCP23017 gpioControl0_2(Wire, 1);
MCP23017 gpioControl0_3(Wire, 1);

MCP23017 gpioControl1(Wire, 3);
MCP23017 gpioControl1_1(Wire, 3);
MCP23017 gpioControl1_2(Wire, 3);
MCP23017 gpioControl1_3(Wire, 3);

MCP23017 gpioControl2(Wire, 5);
MCP23017 gpioControl2_1(Wire, 5);
MCP23017 gpioControl2_2(Wire, 5);
MCP23017 gpioControl2_3(Wire, 5);

MCP23017 gpioControl3(Wire, 7);
MCP23017 gpioControl3_1(Wire, 7);
MCP23017 gpioControl3_2(Wire, 7);
MCP23017 gpioControl3_3(Wire, 7);

char logTopic[64];
 

void setBank(int bank){
  switch(bank){
    case 0:
      digitalWrite(BANK_0, HIGH);
      digitalWrite(BANK_1, LOW);
      digitalWrite(BANK_2, LOW);
      digitalWrite(BANK_3, LOW);
      break;
    case 1:
      digitalWrite(BANK_0, LOW);
      digitalWrite(BANK_1, HIGH);
      digitalWrite(BANK_2, LOW);
      digitalWrite(BANK_3, LOW);
      break;
    case 2:
      digitalWrite(BANK_0, LOW);
      digitalWrite(BANK_1, LOW);
      digitalWrite(BANK_2, HIGH); 
      digitalWrite(BANK_3, LOW);
      break;
    case 3:
      digitalWrite(BANK_0, LOW);
      digitalWrite(BANK_1, LOW);
      digitalWrite(BANK_2, LOW);
      digitalWrite(BANK_3, HIGH);
      break;
  }
}

void initBanks(){
  for(int i = 0; i < 4; i++){
    setBank(i);
    switch(i){
      case 0:
        gpioData0.begin();
        gpioData1.begin();
        gpioData2.begin();
        gpioData3.begin();
        gpioControl0.begin();
        gpioControl1.begin();
        gpioControl2.begin();
        gpioControl3.begin();
        break;
      case 1:
        gpioData0_1.begin();
        gpioData1_1.begin();
        gpioData2_1.begin();
        gpioData3_1.begin();
        gpioControl0_1.begin();
        gpioControl1_1.begin();
        gpioControl2_1.begin();
        gpioControl3_1.begin();
        break;
      case 2:
        gpioData0_2.begin();
        gpioData1_2.begin();
        gpioData2_2.begin();
        gpioData3_2.begin();
        gpioControl0_2.begin();
        gpioControl1_2.begin();
        gpioControl2_2.begin();
        gpioControl3_2.begin();
        break;
      case 3:
        gpioData0_3.begin();
        gpioData1_3.begin();
        gpioData2_3.begin();
        gpioData3_3.begin();
        gpioControl0_3.begin();
        gpioControl1_3.begin();
        gpioControl2_3.begin();
        gpioControl3_3.begin();
        break;
    }
  }
}


bool firstPublish = false;
int drivingComponent = -1;
const char* blynkAPI;
std::string photonID = "0";

//Thread blynkThread;

#define busInputPin V0
#define busValueOutputPin V1
#define uuidInputPin V3
#define busPositionSelectionPin V5
#define resetPin V6
#define uuidResetPin V9
#define ctrlLine0 V10
#define ctrlLine1 V11
#define ctrlLine2 V12
#define ctrlLine3 V13
#define ctrlLine4 V14
#define ctrlLine5 V15
#define ctrlLine6 V16
#define ctrlLine7 V17
#define photonIDinput V18
#define drivingBusCompBlynk V4

#define clockPin D7
#define MCP23017_RESET D6
#define blynkClock V2
int busValue;
std::string currentUUID = "";
int componentSelection;
int components [32][8];
std::string topic = "WCL-CPU/";
int ALUcomponents [32];

int bitExtracted(int number, int k, int p) 
{ 
    return (((1 << k) - 1) & (number >> (p - 1))); 
} 

void assignCtrlLines(int component, int ctrlLine) {
  int mask = 0x01;
  for(int i = 0; i < 8; i++){
    if(ctrlLine & mask){
      components[component][i] = 1;
    }
    else{
      components[component][i] = 0;
    }
    mask = mask << 1;
  }
}

void gpioDatadigitalWrite(int position, bool value){
  for(int bank = 0; bank < 4; bank++){
    setBank(bank);
    gpioData0.digitalWrite(position, value);
    gpioData1.digitalWrite(position, value);
    gpioData2.digitalWrite(position, value);
    gpioData3.digitalWrite(position, value);
  }
  setBank(0);
}

void resetHardware() {
  generateUUID();
  componentSelection = 0;
  Blynk.virtualWrite(busPositionSelectionPin, componentSelection);
  busValue = 0;
  Blynk.virtualWrite(busInputPin, busValue);
  Blynk.virtualWrite(busValueOutputPin, busValue);
}

void setUUID(std::string uuidGiven) {
  Serial.println("setting uuid from setUUID");
  currentUUID = uuidGiven;
  //EEPROM.put(50, currentUUID.c_str());
  Blynk.virtualWrite(uuidInputPin, currentUUID.c_str());
  subscribeToUUID();
  publishToMQTT();
}

BLYNK_WRITE(busInputPin) {
  int previousDrivingComponent = drivingComponent;
  drivingComponent = -1; //fixes issue with MCPs not getting set
  busValue = param.asInt();
  Blynk.virtualWrite(busValueOutputPin, busValue);
  setHardware();
  drivingComponent = previousDrivingComponent;
  //publishToMQTT();
}
BLYNK_WRITE(uuidInputPin) {
  setUUID(param.asString());
  publishToMQTT();
}
BLYNK_WRITE(drivingBusCompBlynk){
  drivingComponent = param.asInt();
  char buffer[64];
  sprintf(buffer, "CPU-WCL/%s/logs", System.deviceID().c_str());
  char payloadBuffer[64];
  sprintf(payloadBuffer, "bus scanning - %i", drivingComponent);
  client.publish(buffer, payloadBuffer);
  scanBusBlynk();
}
BLYNK_WRITE(uuidResetPin){
  generateUUID();
}
BLYNK_WRITE(photonIDinput){
  photonID = param.asString();
  Serial.print("photonID: ");
  Serial.println(photonID.c_str());
}
BLYNK_WRITE(resetPin) {
  resetHardware();
}
BLYNK_WRITE(busPositionSelectionPin) {
  componentSelection = param.asInt();
  Blynk.virtualWrite(ctrlLine0, components[componentSelection][0]);
  Blynk.virtualWrite(ctrlLine1, components[componentSelection][1]);
  Blynk.virtualWrite(ctrlLine2, components[componentSelection][2]);
  Blynk.virtualWrite(ctrlLine3, components[componentSelection][3]);
  Blynk.virtualWrite(ctrlLine4, components[componentSelection][4]);
  Blynk.virtualWrite(ctrlLine5, components[componentSelection][5]);
  Blynk.virtualWrite(ctrlLine6, components[componentSelection][6]);
  Blynk.virtualWrite(ctrlLine7, components[componentSelection][7]);
}
BLYNK_WRITE(blynkClock) {

  char buffer[64];
  sprintf(buffer, "CPU-WCL/%s/logs", System.deviceID().c_str());
  char payloadBuffer[64];
  sprintf(payloadBuffer, "bus scanning - %i", drivingComponent);
  client.publish(buffer, payloadBuffer);
  scanBusBlynk();
  digitalWrite(clockPin, HIGH);
  //publishToMQTT();
  digitalWrite(clockPin, LOW);
  setHardware();
  updateBlynk();
  //TODO add clock MQTT information
}

void scanBusBlynk() {
  char buffer[64];
  sprintf(buffer, "CPU-WCL/%s/logs", System.deviceID().c_str());
  char payloadBuffer[64];
  sprintf(payloadBuffer, "bus scanning - %i", drivingComponent);
  //client.publish(buffer, payloadBuffer);
  if(drivingComponent != -1){
    uint8_t busInBits[16];
    for(int i = 0; i < 16; i++){
      busInBits[i] = 0;
    }
    if(drivingComponent / 8 == 0){
      Serial.println("BLYNK - BANK 0");
      setBank(0);
    }
    else if(drivingComponent / 8 == 1){
      Serial.println("BLYNK - BANK 1");
      setBank(1);
    }
    else if(drivingComponent / 8 == 2){
      Serial.println("BLYNK - BANK 2");
      setBank(2);
    }
    else{
      Serial.println("BLYNK - BANK 3");
      setBank(3);
    }
    int selectedComponent = drivingComponent - ((drivingComponent / 8) * 8);
    if(selectedComponent == 1 || selectedComponent == 0){
      Serial.printf("BLYNK - COMP %i\n", selectedComponent);
      for(int i = 0; i < 16; ++i){
        gpioData0.pinMode(i, INPUT);
        if(i < 8){
          busInBits[i] = gpioData0.digitalRead(i);
        }
        else{
          busInBits[i] = 0;
        }
        gpioData0.pinMode(i, OUTPUT);
      }
      //char buffer[64];
      //sprintf(buffer, "CPU-WCL/%s/logs", System.deviceID().c_str());
      //client.publish(buffer, )
    }
    else if(selectedComponent == 2 || selectedComponent == 3){
      Serial.println("BLYNK - COMP 2");
      for(int i = 0; i < 16; ++i){
        gpioData1.pinMode(i, INPUT);
        if(i < 8){
          busInBits[i] = gpioData1.digitalRead(i);
        }
        else{
          busInBits[i] = 0;
        }
        gpioData1.pinMode(i, OUTPUT);
      }
    }
    else if(selectedComponent == 4 || selectedComponent == 5){
      Serial.println("BLYNK - COMP 4");
      for(int i = 0; i < 16; ++i){
        gpioData2.pinMode(i, INPUT);
        if(i < 8){
          busInBits[i] = gpioData2.digitalRead(i);
        }
        else{
          busInBits[i] = 0;
        }
        gpioData2.pinMode(i, OUTPUT);
      }
    }
    else if(selectedComponent == 6 || selectedComponent == 7){
      Serial.println("BLYNK - COMP 6");
      for(int i = 0; i < 16; ++i){
        gpioData3.pinMode(i, INPUT);
        if(i < 8){
          busInBits[i] = gpioData3.digitalRead(i);
        }
        else{
          busInBits[i] = 0;
        }
        gpioData3.pinMode(i, OUTPUT);
      }
    }
    else{
      for(int i = 0; i < 16; ++i){
        busInBits[i] = 0;
      }
    }
    int bin = 0;
    for(int i = 0; i < 16; ++i){
      Serial.print(busInBits[15-i]);
      if(busInBits[15-i]){
        bin |= 1 << (15 - i);
      }
    }
    Serial.println();
    busValue = bin;
    char payloadBuffer[64];
    sprintf(payloadBuffer, "newBusValue - %i", busValue);
    client.publish(logTopic, payloadBuffer);
  }
  updateBlynk();
  setBank(0);
  setHardware();
}

BLYNK_WRITE(ctrlLine0) {
  components[componentSelection][0] = param.asInt();
  setHardware();
  client.publish(logTopic, param.asString());
  scanBusBlynk();
  //publishToMQTT();
}
BLYNK_WRITE(ctrlLine1) {
  components[componentSelection][1] = param.asInt();
  setHardware();
  scanBusBlynk();
  //publishToMQTT();
}
BLYNK_WRITE(ctrlLine2) {
  components[componentSelection][2] = param.asInt();
  setHardware();
  scanBusBlynk();
  //publishToMQTT();
}
BLYNK_WRITE(ctrlLine3) {
  components[componentSelection][3] = param.asInt();
  setHardware();
  scanBusBlynk();
  //publishToMQTT();
}
BLYNK_WRITE(ctrlLine4) {
  components[componentSelection][4] = param.asInt();
  setHardware();
  scanBusBlynk();
  //publishToMQTT();
}
BLYNK_WRITE(ctrlLine5) {
  components[componentSelection][5] = param.asInt();
  setHardware();
  scanBusBlynk();
  //publishToMQTT();
}
BLYNK_WRITE(ctrlLine6) {
  components[componentSelection][6] = param.asInt();
  setHardware();
  scanBusBlynk();
  //publishToMQTT();
}
BLYNK_WRITE(ctrlLine7) {
  components[componentSelection][7] = param.asInt();
  setHardware();
  scanBusBlynk();
  //publishToMQTT();
}

void uuidHandler(const char *event, const char *data) {
  setUUID(data);
}

void updateBlynk() {
  Blynk.virtualWrite(busInputPin, busValue);
  Blynk.virtualWrite(drivingBusCompBlynk, drivingComponent);
  Blynk.virtualWrite(busValueOutputPin, busValue);
  Blynk.virtualWrite(uuidInputPin, currentUUID.c_str());
  Blynk.virtualWrite(busPositionSelectionPin, componentSelection);
  Blynk.virtualWrite(ctrlLine0, components[componentSelection][0]);
  Blynk.virtualWrite(ctrlLine1, components[componentSelection][1]);
  Blynk.virtualWrite(ctrlLine2, components[componentSelection][2]);
  Blynk.virtualWrite(ctrlLine3, components[componentSelection][3]);
  Blynk.virtualWrite(ctrlLine4, components[componentSelection][4]);
  Blynk.virtualWrite(ctrlLine5, components[componentSelection][5]);
  Blynk.virtualWrite(ctrlLine6, components[componentSelection][6]);
  Blynk.virtualWrite(ctrlLine7, components[componentSelection][7]);
}

void blynkUpdateThread() {
  while(1){
    //Serial.println("blynkupdatethread - updaing");
    Blynk.run();
  }
}

void callback(char* topic, byte* payload, unsigned int length) 
{
  Serial.println(topic);
  Serial.println("MQTT CALLBACK");
  std::string topicArray[6];
  char input[100] = "";
  strcpy(input, topic);
  char* token = strtok(input, "/");
  int iterator = 0;
  while (token) {
      topicArray[iterator] = token;
      token = strtok(NULL, "/");
      iterator++;
  }
  if(topicArray[0] == "CPU-WCL"){
    if (topicArray[1] == currentUUID){
      if (topicArray[2] == "busValue") {
        char p[length + 1];
        memcpy(p, payload, length);
        p[length] = NULL;
        int value = atoi(p);
        busValue = value;
      }
      else if(topicArray[2] == "ALU"){
        if(topicArray[3] == photonID){
          Serial.println("PHOTON ID VALID FOR ALU");
          Serial.println(atoi(topicArray[4].c_str()));
          ALUcomponents[atoi(topicArray[4].c_str())] = 1;
        }
      }
      else if(topicArray[2] == "blynkAPI") {
        for(int pos = 0; pos < 32; ++pos){
          EEPROM.put((pos + 3), payload[pos]);
          Serial.println(payload[pos]);
        }
        const char* payloadtest;
        Serial.println((const char*)payload);
        EEPROM.get(3, payloadtest);
        blynkAPI = (const char*)payload;
        Blynk.config(blynkAPI, IPAddress(167,172,234,162),9090);
        Blynk.connect();
        updateBlynk();
      }
      //component ctrlLines
      else if (topicArray[2] == "components") {
        //Serial.println("COMPONENTS");
        
        if(topicArray[3] == photonID){
          Serial.println("topic and stored photon ID match");
          char p[length + 1];
          memcpy(p, payload, length);
          p[length] = NULL;
          int status = atoi(p);
          int comp = std::atoi((topicArray[4].c_str()));
          assignCtrlLines(comp, status);
          for(int i = 0; i < 8; i++){
            Serial.print(components[comp][i]);
          }
          Serial.println("");
        }
        else{
          Serial.print("photon ID does not match - ID: ");
          Serial.print(topicArray[3].c_str());
          Serial.print(" ");
          Serial.println(photonID.c_str());
        }
      }
      else if (topicArray[2] == "busValueDown") {
        char p[length + 1];
        memcpy(p, payload, length);
        p[length] = NULL;
        busValue = std::atoi(p);
      }
      else if(topicArray[2] == "clockPulse") {
        mqttClockPulse();
      }
      else if (topicArray[2] == "drivingBus") {
        char p[length + 1];
        memcpy(p, payload, length);
        p[length] = NULL;
        if(-1 != std::atoi(p)){
          Serial.print("drivingBus = ");
          Serial.println(std::atoi(p));
          drivingComponent = std::atoi(p);
        }
        else {
          Serial.println("drivingBus = -1");
          drivingComponent = -1;
        }
      }
    }
    else if(topicArray[1] == System.deviceID().c_str()){
      char subTopic[64];
      sprintf(subTopic, "CPU-WCL/%s/logs", System.deviceID().c_str());
      Serial.println(subTopic);
      client.publish(subTopic, "clearing EEPROM");
      EEPROM.clear();
      System.reset();
    }
  }
  else if(topicArray[0] == "uuidgenResult"){
    char p[length +1];
    memcpy(p, payload, length);
    for(int pos = 0; pos < 36; ++pos){
      EEPROM.put((pos + 50), payload[pos]);
      Serial.println(payload[pos]);
    }
    p[length] = NULL;
    std::string uuid(p, length);
    Serial.println(uuid.c_str());
    char subTopic[64];
    sprintf(subTopic, "CPU-WCL/%s/logs", System.deviceID().c_str());
    Serial.println(subTopic);
    client.publish(subTopic, uuid.c_str());
    setUUID(uuid);
    subscribeToUUID();
    client.unsubscribe("uuidgenResult");
  }
  setHardware();
}

void readCtrlLines() {
  digitalWrite(BANK_0, HIGH);
  digitalWrite(BANK_1, LOW);
  digitalWrite(BANK_2, LOW);
  digitalWrite(BANK_3, LOW);
    for(int i = 0; i < 8; ++i) {
      gpioControl0.pinMode(i, INPUT);
      components[0][i] = gpioControl0.digitalRead(i);
      gpioControl0.pinMode(i, OUTPUT);
      gpioControl0.pinMode(i + 8, INPUT);
      components[1][i] = gpioControl0.digitalRead(i + 8);
      gpioControl0.pinMode(i + 8, OUTPUT);
      gpioControl1.pinMode(i, INPUT);
      gpioControl1.pinMode(i + 8, INPUT);
      components[2][i] = gpioControl1.digitalRead(i);
      components[3][i] = gpioControl1.digitalRead(i + 8);
      gpioControl1.pinMode(i, OUTPUT);
      gpioControl1.pinMode(i + 8, OUTPUT);
      gpioControl2.pinMode(i, INPUT);
      gpioControl2.pinMode(i + 8, INPUT);
      components[4][i] = gpioControl2.digitalRead(i);
      components[5][i] = gpioControl2.digitalRead(i + 8);
      gpioControl2.pinMode(i, OUTPUT);
      gpioControl2.pinMode(i + 8, OUTPUT);
      gpioControl3.pinMode(i, INPUT);
      gpioControl3.pinMode(i + 8, INPUT);
      components[6][i] = gpioControl3.digitalRead(i);
      components[7][i] = gpioControl3.digitalRead(i + 8);
      gpioControl3.pinMode(i, OUTPUT);
      gpioControl3.pinMode(i + 8, OUTPUT);
    }
  uint8_t busInBits[16];
  for(int i = 0; i < 16; ++i){
    gpioData0.pinMode(i, INPUT);
    gpioData1.pinMode(i, INPUT);
    gpioData2.pinMode(i, INPUT);
    gpioData3.pinMode(i, INPUT);
    busInBits[i] = !gpioData0.digitalRead(i);
    gpioData0.pinMode(i, OUTPUT);
    gpioData1.pinMode(i, OUTPUT);
    gpioData2.pinMode(i, OUTPUT);
    gpioData3.pinMode(i, OUTPUT);
  }
  int i = 0;
  for (int bin = 0, i = 0; i < 16; ++i)
  {
    bin *= 2;
    bin = bin + busInBits[i];
    if(i == 15) {
      busValue = bin;
    }
  }
  Blynk.virtualWrite(busValueOutputPin, busValue);
  Blynk.virtualWrite(busInputPin, busValue);
}

void mqttClockPulse() {
  Serial.println(drivingComponent);
  digitalWrite(clockPin, HIGH);
  delay(50);
  digitalWrite(clockPin, LOW);
  for(int component = 0; component < 32; component++){
    if(ALUcomponents[component] == 1){
      Serial.println("---ALU---");
      if(component < 8){
        Serial.println("ALU - bank0");
        setBank(0);
      }
      else if(component < 16 && component > 7){
        setBank(1);
        Serial.println("ALU - bank1");
      }
      else if(component < 24 && component > 15){
        Serial.println("ALU - bank2");
        setBank(2);
      }
      else if(component < 32 && component > 23){
        Serial.println("ALU - bank3");
        setBank(3);
      }
      if(component - (component / 8) == 0){
        for(int i = 0; i < 8; ++i){
          gpioControl0.pinMode(i, INPUT);
          components[component][i] = gpioControl0.digitalRead(i);
          gpioControl0.pinMode(i, OUTPUT);
        }
      }
      else if(component - (component / 8) == 1){
        Serial.print("ALU READ - ");
        for(int i = 0; i < 8; ++i){
          gpioControl0.pinMode(i + 8, INPUT);
          components[component][i] = gpioControl0.digitalRead(i + 8);
          Serial.print(components[component][i]);
          gpioControl0.pinMode(i + 8, OUTPUT);
        }
        Serial.println("");
      }
      else if(component - (component / 8) == 2){
        for(int i = 0; i < 8; ++i){
          gpioControl1.pinMode(i, INPUT);
          components[component][i] = gpioControl1.digitalRead(i);
          gpioControl1.pinMode(i, OUTPUT);
        }
      }
      else if(component - (component / 8) == 3){
        for(int i = 0; i < 8; ++i){
          gpioControl1.pinMode(i + 8, INPUT);
          components[component][i] = gpioControl1.digitalRead(i + 8);
          gpioControl1.pinMode(i + 8, OUTPUT);
        }
      }
      else if(component - (component / 8) == 4){
        for(int i = 0; i < 8; ++i){
          gpioControl2.pinMode(i, INPUT);
          components[component][i] = gpioControl2.digitalRead(i);
          gpioControl2.pinMode(i, OUTPUT);
        }
      }
      else if(component - (component / 8) == 5){
        for(int i = 0; i < 8; ++i){
          gpioControl2.pinMode(i + 8, INPUT);
          components[component][i] = gpioControl2.digitalRead(i + 8);
          gpioControl2.pinMode(i + 8, OUTPUT);
        }
      }
      else if(component - (component / 8) == 6){
        for(int i = 0; i < 8; ++i){
          gpioControl3.pinMode(i, INPUT);
          components[component][i] = gpioControl3.digitalRead(i);
          gpioControl3.pinMode(i, OUTPUT);
        }
      }
      else if(component - (component / 8) == 7){
        Serial.print("ALU READ - ");
        for(int i = 0; i < 8; ++i){
          gpioControl3.pinMode(i + 8, INPUT);
          components[component][i] = gpioControl3.digitalRead(i + 8);
          Serial.print(gpioControl3.digitalRead(i + 8));
          gpioControl3.pinMode(i + 8, OUTPUT);
        }
        Serial.println("");
      }
      int bin = 0;
      for(int i = 0; i < 8; ++i){
        Serial.print(components[component][7-i]);
        if(components[component][7-i]){
          bin |= 1 << (7 - i);
        }
      }
      Serial.println("");
      char topicBuffer[64];
      char payload[8];
      itoa(bin, payload, 10);
      Serial.print("ALU - ");
      Serial.println(payload);
      sprintf(topicBuffer, "CPU-WCL/%s/components/%s/%d/controlUp", currentUUID.c_str(), photonID.c_str(), component);
      client.publish(topicBuffer, payload);
    }
  }
  if(drivingComponent != -1) {
    Serial.print("mqttClock - drivingcomponent = ");
    Serial.println(drivingComponent);
    uint8_t busInBits[16];
    if(drivingComponent / 8 == 0){
      //BANK 0
      digitalWrite(BANK_0, HIGH);
      digitalWrite(BANK_1, LOW);
      digitalWrite(BANK_2, LOW);
      digitalWrite(BANK_3, LOW);
      if(drivingComponent == 1 || drivingComponent == 0){
        Serial.println("0, 1");
        for(int i = 0; i < 16; ++i){
          gpioData0.pinMode(i, INPUT);
        }
        busInBits[0] = gpioData0.digitalRead(15);
        busInBits[1] = gpioData0.digitalRead(14);
        busInBits[2] = gpioData0.digitalRead(13);
        busInBits[3] = gpioData0.digitalRead(12);
        busInBits[4] = gpioData0.digitalRead(11);
        busInBits[5] = gpioData0.digitalRead(10);
        busInBits[6] = gpioData0.digitalRead(9);
        busInBits[7] = gpioData0.digitalRead(8);
        busInBits[8] = gpioData0.digitalRead(7);
        busInBits[9] = gpioData0.digitalRead(6);
        busInBits[10] = gpioData0.digitalRead(5);
        busInBits[11] = gpioData0.digitalRead(4);
        busInBits[12] = gpioData0.digitalRead(3);
        busInBits[13] = gpioData0.digitalRead(2);
        busInBits[14] = gpioData0.digitalRead(1);
        busInBits[15] = gpioData0.digitalRead(0);
        for(int i = 0; i < 16; ++i){
          gpioData0.pinMode(i, OUTPUT);
        }
      }
      else if(drivingComponent == 2 || drivingComponent == 3){
        Serial.println("2, 3");
        for(int i = 0; i < 16; ++i){
          gpioData1.pinMode(i, INPUT);
        }
        busInBits[0] = gpioData1.digitalRead(15);
        busInBits[1] = gpioData1.digitalRead(14);
        busInBits[2] = gpioData1.digitalRead(13);
        busInBits[3] = gpioData1.digitalRead(12);
        busInBits[4] = gpioData1.digitalRead(11);
        busInBits[5] = gpioData1.digitalRead(10);
        busInBits[6] = gpioData1.digitalRead(9);
        busInBits[7] = gpioData1.digitalRead(8);
        busInBits[8] = gpioData1.digitalRead(7);
        busInBits[9] = gpioData1.digitalRead(6);
        busInBits[10] = gpioData1.digitalRead(5);
        busInBits[11] = gpioData1.digitalRead(4);
        busInBits[12] = gpioData1.digitalRead(3);
        busInBits[13] = gpioData1.digitalRead(2);
        busInBits[14] = gpioData1.digitalRead(1);
        busInBits[15] = gpioData1.digitalRead(0);
        for(int i = 0; i < 16; ++i){
          gpioData1.pinMode(i, OUTPUT);
        }
      }
      else if(drivingComponent == 4 || drivingComponent == 5){
        Serial.println("4, 5");
        for(int i = 0; i < 16; ++i){
          gpioData2.pinMode(i, INPUT);
        }
        busInBits[0] = gpioData2.digitalRead(15);
        busInBits[1] = gpioData2.digitalRead(14);
        busInBits[2] = gpioData2.digitalRead(13);
        busInBits[3] = gpioData2.digitalRead(12);
        busInBits[4] = gpioData2.digitalRead(11);
        busInBits[5] = gpioData2.digitalRead(10);
        busInBits[6] = gpioData2.digitalRead(9);
        busInBits[7] = gpioData2.digitalRead(8);
        busInBits[8] = gpioData2.digitalRead(7);
        busInBits[9] = gpioData2.digitalRead(6);
        busInBits[10] = gpioData2.digitalRead(5);
        busInBits[11] = gpioData2.digitalRead(4);
        busInBits[12] = gpioData2.digitalRead(3);
        busInBits[13] = gpioData2.digitalRead(2);
        busInBits[14] = gpioData2.digitalRead(1);
        busInBits[15] = gpioData2.digitalRead(0);
        for(int i = 0; i < 16; ++i){
          gpioData2.pinMode(i, OUTPUT);
        }
      }
      else if(drivingComponent == 6 || drivingComponent == 7){
        Serial.println("6, 7");
        for(int i = 0; i < 16; ++i){
          gpioData3.pinMode(i, INPUT);
        }
        busInBits[0] = gpioData3.digitalRead(15);
        busInBits[1] = gpioData3.digitalRead(14);
        busInBits[2] = gpioData3.digitalRead(13);
        busInBits[3] = gpioData3.digitalRead(12);
        busInBits[4] = gpioData3.digitalRead(11);
        busInBits[5] = gpioData3.digitalRead(10);
        busInBits[6] = gpioData3.digitalRead(9);
        busInBits[7] = gpioData3.digitalRead(8);
        busInBits[8] = gpioData3.digitalRead(7);
        busInBits[9] = gpioData3.digitalRead(6);
        busInBits[10] = gpioData3.digitalRead(5);
        busInBits[11] = gpioData3.digitalRead(4);
        busInBits[12] = gpioData3.digitalRead(3);
        busInBits[13] = gpioData3.digitalRead(2);
        busInBits[14] = gpioData3.digitalRead(1);
        busInBits[15] = gpioData3.digitalRead(0);
        for(int i = 0; i < 16; ++i){
          gpioData3.pinMode(i, OUTPUT);
        }
      }
      digitalWrite(BANK_0, HIGH);
      digitalWrite(BANK_1, LOW);
      digitalWrite(BANK_2, LOW);
      digitalWrite(BANK_3, LOW);
    }
    else if(drivingComponent / 8 == 1){
      //BANK 1
      digitalWrite(BANK_0, LOW);
      digitalWrite(BANK_1, HIGH);
      digitalWrite(BANK_2, LOW);
      digitalWrite(BANK_3, LOW);
      if(drivingComponent == 8 || drivingComponent == 9){
        Serial.println("0, 1");
        for(int i = 0; i < 16; ++i){
          gpioData0.pinMode(i, INPUT);
        }
        busInBits[0] = gpioData0.digitalRead(15);
        busInBits[1] = gpioData0.digitalRead(14);
        busInBits[2] = gpioData0.digitalRead(13);
        busInBits[3] = gpioData0.digitalRead(12);
        busInBits[4] = gpioData0.digitalRead(11);
        busInBits[5] = gpioData0.digitalRead(10);
        busInBits[6] = gpioData0.digitalRead(9);
        busInBits[7] = gpioData0.digitalRead(8);
        busInBits[8] = gpioData0.digitalRead(7);
        busInBits[9] = gpioData0.digitalRead(6);
        busInBits[10] = gpioData0.digitalRead(5);
        busInBits[11] = gpioData0.digitalRead(4);
        busInBits[12] = gpioData0.digitalRead(3);
        busInBits[13] = gpioData0.digitalRead(2);
        busInBits[14] = gpioData0.digitalRead(1);
        busInBits[15] = gpioData0.digitalRead(0);
        for(int i = 0; i < 16; ++i){
          gpioData0.pinMode(i, OUTPUT);
        }
      }
      else if(drivingComponent == 10 || drivingComponent == 11){
        Serial.println("2, 3");
        for(int i = 0; i < 16; ++i){
          gpioData1.pinMode(i, INPUT);
        }
        busInBits[0] = gpioData1.digitalRead(15);
        busInBits[1] = gpioData1.digitalRead(14);
        busInBits[2] = gpioData1.digitalRead(13);
        busInBits[3] = gpioData1.digitalRead(12);
        busInBits[4] = gpioData1.digitalRead(11);
        busInBits[5] = gpioData1.digitalRead(10);
        busInBits[6] = gpioData1.digitalRead(9);
        busInBits[7] = gpioData1.digitalRead(8);
        busInBits[8] = gpioData1.digitalRead(7);
        busInBits[9] = gpioData1.digitalRead(6);
        busInBits[10] = gpioData1.digitalRead(5);
        busInBits[11] = gpioData1.digitalRead(4);
        busInBits[12] = gpioData1.digitalRead(3);
        busInBits[13] = gpioData1.digitalRead(2);
        busInBits[14] = gpioData1.digitalRead(1);
        busInBits[15] = gpioData1.digitalRead(0);
        for(int i = 0; i < 16; ++i){
          gpioData1.pinMode(i, OUTPUT);
        }
      }
      else if(drivingComponent == 12 || drivingComponent == 13){
        Serial.println("4, 5");
        for(int i = 0; i < 16; ++i){
          gpioData2.pinMode(i, INPUT);
        }
        busInBits[0] = gpioData2.digitalRead(15);
        busInBits[1] = gpioData2.digitalRead(14);
        busInBits[2] = gpioData2.digitalRead(13);
        busInBits[3] = gpioData2.digitalRead(12);
        busInBits[4] = gpioData2.digitalRead(11);
        busInBits[5] = gpioData2.digitalRead(10);
        busInBits[6] = gpioData2.digitalRead(9);
        busInBits[7] = gpioData2.digitalRead(8);
        busInBits[8] = gpioData2.digitalRead(7);
        busInBits[9] = gpioData2.digitalRead(6);
        busInBits[10] = gpioData2.digitalRead(5);
        busInBits[11] = gpioData2.digitalRead(4);
        busInBits[12] = gpioData2.digitalRead(3);
        busInBits[13] = gpioData2.digitalRead(2);
        busInBits[14] = gpioData2.digitalRead(1);
        busInBits[15] = gpioData2.digitalRead(0);
        for(int i = 0; i < 16; ++i){
          gpioData2.pinMode(i, OUTPUT);
        }
      }
      else if(drivingComponent == 14 || drivingComponent == 15){
        Serial.println("6, 7");
        for(int i = 0; i < 16; ++i){
          gpioData3.pinMode(i, INPUT);
        }
        busInBits[0] = gpioData3.digitalRead(15);
        busInBits[1] = gpioData3.digitalRead(14);
        busInBits[2] = gpioData3.digitalRead(13);
        busInBits[3] = gpioData3.digitalRead(12);
        busInBits[4] = gpioData3.digitalRead(11);
        busInBits[5] = gpioData3.digitalRead(10);
        busInBits[6] = gpioData3.digitalRead(9);
        busInBits[7] = gpioData3.digitalRead(8);
        busInBits[8] = gpioData3.digitalRead(7);
        busInBits[9] = gpioData3.digitalRead(6);
        busInBits[10] = gpioData3.digitalRead(5);
        busInBits[11] = gpioData3.digitalRead(4);
        busInBits[12] = gpioData3.digitalRead(3);
        busInBits[13] = gpioData3.digitalRead(2);
        busInBits[14] = gpioData3.digitalRead(1);
        busInBits[15] = gpioData3.digitalRead(0);
        for(int i = 0; i < 16; ++i){
          gpioData3.pinMode(i, OUTPUT);
        }
      }
      digitalWrite(BANK_0, HIGH);
      digitalWrite(BANK_1, LOW);
      digitalWrite(BANK_2, LOW);
      digitalWrite(BANK_3, LOW);
    }
    else if(drivingComponent / 8 == 2){
      //BANK 2
      digitalWrite(BANK_0, LOW);
      digitalWrite(BANK_1, LOW);
      digitalWrite(BANK_2, HIGH);
      digitalWrite(BANK_3, LOW);
      if(drivingComponent == 16 || drivingComponent == 17){
        Serial.println("0, 1");
        for(int i = 0; i < 16; ++i){
          gpioData0.pinMode(i, INPUT);
        }
        busInBits[0] = gpioData0.digitalRead(15);
        busInBits[1] = gpioData0.digitalRead(14);
        busInBits[2] = gpioData0.digitalRead(13);
        busInBits[3] = gpioData0.digitalRead(12);
        busInBits[4] = gpioData0.digitalRead(11);
        busInBits[5] = gpioData0.digitalRead(10);
        busInBits[6] = gpioData0.digitalRead(9);
        busInBits[7] = gpioData0.digitalRead(8);
        busInBits[8] = gpioData0.digitalRead(7);
        busInBits[9] = gpioData0.digitalRead(6);
        busInBits[10] = gpioData0.digitalRead(5);
        busInBits[11] = gpioData0.digitalRead(4);
        busInBits[12] = gpioData0.digitalRead(3);
        busInBits[13] = gpioData0.digitalRead(2);
        busInBits[14] = gpioData0.digitalRead(1);
        busInBits[15] = gpioData0.digitalRead(0);
        for(int i = 0; i < 16; ++i){
          gpioData0.pinMode(i, OUTPUT);
        }
      }
      else if(drivingComponent == 18 || drivingComponent == 19){
        Serial.println("2, 3");
        for(int i = 0; i < 16; ++i){
          gpioData1.pinMode(i, INPUT);
        }
        busInBits[0] = gpioData1.digitalRead(15);
        busInBits[1] = gpioData1.digitalRead(14);
        busInBits[2] = gpioData1.digitalRead(13);
        busInBits[3] = gpioData1.digitalRead(12);
        busInBits[4] = gpioData1.digitalRead(11);
        busInBits[5] = gpioData1.digitalRead(10);
        busInBits[6] = gpioData1.digitalRead(9);
        busInBits[7] = gpioData1.digitalRead(8);
        busInBits[8] = gpioData1.digitalRead(7);
        busInBits[9] = gpioData1.digitalRead(6);
        busInBits[10] = gpioData1.digitalRead(5);
        busInBits[11] = gpioData1.digitalRead(4);
        busInBits[12] = gpioData1.digitalRead(3);
        busInBits[13] = gpioData1.digitalRead(2);
        busInBits[14] = gpioData1.digitalRead(1);
        busInBits[15] = gpioData1.digitalRead(0);
        for(int i = 0; i < 16; ++i){
          gpioData1.pinMode(i, OUTPUT);
        }
      }
      else if(drivingComponent == 20 || drivingComponent == 21){
        Serial.println("4, 5");
        for(int i = 0; i < 16; ++i){
          gpioData2.pinMode(i, INPUT);
        }
        busInBits[0] = gpioData2.digitalRead(15);
        busInBits[1] = gpioData2.digitalRead(14);
        busInBits[2] = gpioData2.digitalRead(13);
        busInBits[3] = gpioData2.digitalRead(12);
        busInBits[4] = gpioData2.digitalRead(11);
        busInBits[5] = gpioData2.digitalRead(10);
        busInBits[6] = gpioData2.digitalRead(9);
        busInBits[7] = gpioData2.digitalRead(8);
        busInBits[8] = gpioData2.digitalRead(7);
        busInBits[9] = gpioData2.digitalRead(6);
        busInBits[10] = gpioData2.digitalRead(5);
        busInBits[11] = gpioData2.digitalRead(4);
        busInBits[12] = gpioData2.digitalRead(3);
        busInBits[13] = gpioData2.digitalRead(2);
        busInBits[14] = gpioData2.digitalRead(1);
        busInBits[15] = gpioData2.digitalRead(0);
        for(int i = 0; i < 16; ++i){
          gpioData2.pinMode(i, OUTPUT);
        }
      }
      else if(drivingComponent == 22 || drivingComponent == 23){
        Serial.println("6, 7");
        for(int i = 0; i < 16; ++i){
          gpioData3.pinMode(i, INPUT);
        }
        busInBits[0] = gpioData3.digitalRead(15);
        busInBits[1] = gpioData3.digitalRead(14);
        busInBits[2] = gpioData3.digitalRead(13);
        busInBits[3] = gpioData3.digitalRead(12);
        busInBits[4] = gpioData3.digitalRead(11);
        busInBits[5] = gpioData3.digitalRead(10);
        busInBits[6] = gpioData3.digitalRead(9);
        busInBits[7] = gpioData3.digitalRead(8);
        busInBits[8] = gpioData3.digitalRead(7);
        busInBits[9] = gpioData3.digitalRead(6);
        busInBits[10] = gpioData3.digitalRead(5);
        busInBits[11] = gpioData3.digitalRead(4);
        busInBits[12] = gpioData3.digitalRead(3);
        busInBits[13] = gpioData3.digitalRead(2);
        busInBits[14] = gpioData3.digitalRead(1);
        busInBits[15] = gpioData3.digitalRead(0);
        for(int i = 0; i < 16; ++i){
          gpioData3.pinMode(i, OUTPUT);
        }
      }
      digitalWrite(BANK_0, HIGH);
      digitalWrite(BANK_1, LOW);
      digitalWrite(BANK_2, LOW);
      digitalWrite(BANK_3, LOW);
    }
    else if(drivingComponent / 8 == 3){
      //BANK 3
      digitalWrite(BANK_0, LOW);
      digitalWrite(BANK_1, LOW);
      digitalWrite(BANK_2, LOW);
      digitalWrite(BANK_3, HIGH);
      if(drivingComponent == 24 || drivingComponent == 25){
        Serial.println("0, 1");
        for(int i = 0; i < 16; ++i){
          gpioData0.pinMode(i, INPUT);
        }
        busInBits[0] = gpioData0.digitalRead(15);
        busInBits[1] = gpioData0.digitalRead(14);
        busInBits[2] = gpioData0.digitalRead(13);
        busInBits[3] = gpioData0.digitalRead(12);
        busInBits[4] = gpioData0.digitalRead(11);
        busInBits[5] = gpioData0.digitalRead(10);
        busInBits[6] = gpioData0.digitalRead(9);
        busInBits[7] = gpioData0.digitalRead(8);
        busInBits[8] = gpioData0.digitalRead(7);
        busInBits[9] = gpioData0.digitalRead(6);
        busInBits[10] = gpioData0.digitalRead(5);
        busInBits[11] = gpioData0.digitalRead(4);
        busInBits[12] = gpioData0.digitalRead(3);
        busInBits[13] = gpioData0.digitalRead(2);
        busInBits[14] = gpioData0.digitalRead(1);
        busInBits[15] = gpioData0.digitalRead(0);
        for(int i = 0; i < 16; ++i){
          gpioData0.pinMode(i, OUTPUT);
        }
      }
      else if(drivingComponent == 26 || drivingComponent == 27){
        Serial.println("2, 3");
        for(int i = 0; i < 16; ++i){
          gpioData1.pinMode(i, INPUT);
        }
        busInBits[0] = gpioData1.digitalRead(15);
        busInBits[1] = gpioData1.digitalRead(14);
        busInBits[2] = gpioData1.digitalRead(13);
        busInBits[3] = gpioData1.digitalRead(12);
        busInBits[4] = gpioData1.digitalRead(11);
        busInBits[5] = gpioData1.digitalRead(10);
        busInBits[6] = gpioData1.digitalRead(9);
        busInBits[7] = gpioData1.digitalRead(8);
        busInBits[8] = gpioData1.digitalRead(7);
        busInBits[9] = gpioData1.digitalRead(6);
        busInBits[10] = gpioData1.digitalRead(5);
        busInBits[11] = gpioData1.digitalRead(4);
        busInBits[12] = gpioData1.digitalRead(3);
        busInBits[13] = gpioData1.digitalRead(2);
        busInBits[14] = gpioData1.digitalRead(1);
        busInBits[15] = gpioData1.digitalRead(0);
        for(int i = 0; i < 16; ++i){
          gpioData1.pinMode(i, OUTPUT);
        }
      }
      else if(drivingComponent == 28 || drivingComponent == 29){
        Serial.println("4, 5");
        for(int i = 0; i < 16; ++i){
          gpioData2.pinMode(i, INPUT);
        }
        busInBits[0] = gpioData2.digitalRead(15);
        busInBits[1] = gpioData2.digitalRead(14);
        busInBits[2] = gpioData2.digitalRead(13);
        busInBits[3] = gpioData2.digitalRead(12);
        busInBits[4] = gpioData2.digitalRead(11);
        busInBits[5] = gpioData2.digitalRead(10);
        busInBits[6] = gpioData2.digitalRead(9);
        busInBits[7] = gpioData2.digitalRead(8);
        busInBits[8] = gpioData2.digitalRead(7);
        busInBits[9] = gpioData2.digitalRead(6);
        busInBits[10] = gpioData2.digitalRead(5);
        busInBits[11] = gpioData2.digitalRead(4);
        busInBits[12] = gpioData2.digitalRead(3);
        busInBits[13] = gpioData2.digitalRead(2);
        busInBits[14] = gpioData2.digitalRead(1);
        busInBits[15] = gpioData2.digitalRead(0);
        for(int i = 0; i < 16; ++i){
          gpioData2.pinMode(i, OUTPUT);
        }
      }
      else if(drivingComponent == 30 || drivingComponent == 31){
        Serial.println("6, 7");
        for(int i = 0; i < 16; ++i){
          gpioData3.pinMode(i, INPUT);
        }
        busInBits[0] = gpioData3.digitalRead(15);
        busInBits[1] = gpioData3.digitalRead(14);
        busInBits[2] = gpioData3.digitalRead(13);
        busInBits[3] = gpioData3.digitalRead(12);
        busInBits[4] = gpioData3.digitalRead(11);
        busInBits[5] = gpioData3.digitalRead(10);
        busInBits[6] = gpioData3.digitalRead(9);
        busInBits[7] = gpioData3.digitalRead(8);
        busInBits[8] = gpioData3.digitalRead(7);
        busInBits[9] = gpioData3.digitalRead(6);
        busInBits[10] = gpioData3.digitalRead(5);
        busInBits[11] = gpioData3.digitalRead(4);
        busInBits[12] = gpioData3.digitalRead(3);
        busInBits[13] = gpioData3.digitalRead(2);
        busInBits[14] = gpioData3.digitalRead(1);
        busInBits[15] = gpioData3.digitalRead(0);
        for(int i = 0; i < 16; ++i){
          gpioData3.pinMode(i, OUTPUT);
        }
      }
      digitalWrite(BANK_0, HIGH);
      digitalWrite(BANK_1, LOW);
      digitalWrite(BANK_2, LOW);
      digitalWrite(BANK_3, LOW);
    }
    

    for (int bin = 0, i = 0; i < 16; ++i)
    {
      bin *= 2;
      bin = bin + busInBits[i];
      if(i == 15) {
        busValue = bin % 255;
      }
    }
    Serial.print("busValue - ");
    Serial.println(busValue % 255);
    setHardware();
    drivingComponent = -1;
  }
  
  publishToMQTT();
}

void publishToMQTT() {
  //busvalue
  Serial.println("publishing to MQTT");
  Serial.println(currentUUID.c_str());
  char busValueChar[16];
  sprintf(busValueChar, "%d", busValue);
  char topicBuffer[64];
  sprintf(topicBuffer, "CPU-WCL/%s/busValueUp", currentUUID.c_str());
  Serial.print("publishing - ");
  Serial.println(topicBuffer);
  client.publish(topicBuffer, busValueChar);
  //component ctrl lines 
  /*for (int component = 0; component < 16; ++component) {
    for (int ctrlLine = 0; ctrlLine < 8; ++ctrlLine) {
      char topicBuffer[64];
      sprintf(topicBuffer, "CPU-WCL/%s/components/%d/%d", currentUUID.c_str(), component, ctrlLine);
      char ctrlLineBuffer[1];
      sprintf(ctrlLineBuffer, "%d", components[component][ctrlLine]);
      //client.publish(topicBuffer, ctrlLineBuffer);
    }
  }*/
}

void subscribeToUUID() {
  if(currentUUID != ""){
    char buffer[64];
    sprintf(buffer, "CPU-WCL/%s/#", currentUUID.c_str());
    Serial.println(buffer);
    client.subscribe(buffer);
  }
  client.unsubscribe("uuidgenResult");
  Serial.println("subscribed to uuidgenResult");
  char subTopic[64];
  sprintf(subTopic, "CPU-WCL/%s", System.deviceID().c_str());
  Serial.println(subTopic);
  client.subscribe(subTopic);
}

void blynkConnect(){
  Serial.println("blynk");
  char subTopic[64];
  sprintf(subTopic, "CPU-WCL/%s/logs", System.deviceID().c_str());
  Serial.println(subTopic);
//  client.publish(subTopic, "clearing EEPROM");
//  char deviceLogsTopic[64];
//  sprintf(deviceLogsTopic, "CPU-WCL/%s/logs", System.deviceID().c_str());
  if(EEPROM.read(3) != 0xFF){
    client.publish(subTopic, "blynkAPI defined");
    char blynkKey[33];
    for(int i = 0; i < 32; ++i){
      blynkKey[i] = EEPROM.read(i + 3);
    }
    blynkKey[32] = '\0';
    blynkAPI = (const char*)blynkKey;
    Serial.println("not 0xFF");
    Serial.println(blynkAPI);
    Blynk.begin(blynkAPI,IPAddress(167,172,234,162),9090);
    Serial.println("blynk began");
  }
  else{
    client.publish(subTopic, "blynkAPI not defined using default");
    Serial.println("0xFF");
    Blynk.begin("vKL1pvTJ60pMZjeIoAFgpFJMnqVH97jA",IPAddress(167,172,234,162),9090); //my personal blynk program, here to prevent program hanging
    Serial.println("blynk began (default)");
  }
  Blynk.connect();
  updateBlynk();
  //blynkThread = Thread("blynk", blynkUpdateThread);
}
void setup() {
  sprintf(logTopic, "CPU-WCL/%s/logs", System.deviceID().c_str());
  Particle.variable("busvalue", busValue);
  pinMode(MCP23017_RESET, OUTPUT);
  digitalWrite(MCP23017_RESET, LOW); //reset mcps
  delay(100);
  digitalWrite(MCP23017_RESET, HIGH);
  delay(100);
  client.connect(System.deviceID().c_str());
  Serial.begin(9600);  
  
  //if(EEPROM.read(50) != 0xFF){
  //  EEPROM.get(50, currentUUID);
  //}
  //Particle.subscribe("hook-response/generateUUID", uuidHandler, MY_DEVICES);
  Particle.variable("cpuVersion", cpuVersion.c_str());
  Particle.variable("blynkKey", blynkAPI);
  Particle.variable("uuid", currentUUID.c_str());
  pinMode(BANK_0, OUTPUT); //pinmodes
  pinMode(BANK_1, OUTPUT);
  pinMode(BANK_2, OUTPUT);
  pinMode(BANK_3, OUTPUT);
  pinMode(clockPin, OUTPUT);



  initBanks();
  for(int bank = 0; bank < 4; ++bank){
    setBank(bank);
    for(int i = 0; i < 16; ++i){
      gpioData0.pinMode(i, OUTPUT);
      gpioData1.pinMode(i, OUTPUT);
      gpioData2.pinMode(i, OUTPUT);
      gpioData3.pinMode(i, OUTPUT);
      gpioControl0.pinMode(i, OUTPUT);
      gpioControl1.pinMode(i, OUTPUT);
      gpioControl2.pinMode(i, OUTPUT);
      gpioControl3.pinMode(i, OUTPUT);
    }
  }
  
  
  readCtrlLines();
  blynkConnect();
  //blynkThread = Thread("blynkUpdate", blynkUpdateThread);
  uuidCheck();
  Blynk.virtualWrite(ctrlLine0, components[componentSelection][0]);
  Blynk.virtualWrite(ctrlLine1, components[componentSelection][1]);
  Blynk.virtualWrite(ctrlLine2, components[componentSelection][2]);
  Blynk.virtualWrite(ctrlLine3, components[componentSelection][3]);
  Blynk.virtualWrite(ctrlLine4, components[componentSelection][4]); 
  Blynk.virtualWrite(ctrlLine5, components[componentSelection][5]);
  Blynk.virtualWrite(ctrlLine6, components[componentSelection][6]);
  Blynk.virtualWrite(ctrlLine7, components[componentSelection][7]);
  Particle.variable("uuid", currentUUID.c_str());
}

void generateUUID() {
  Serial.println("generating UUID");
  client.subscribe("uuidgenResult");
  uuidgenPublish();
}

void uuidgenPublish(){
  char id[25];
  const char* cstring = System.deviceID().c_str();
  memcpy(id, cstring, (strlen(cstring) + 1));
  Serial.println(id);
  if(client.isConnected()){
    client.publish("uuidgen", id);
  }
  else {
    bool wait = true;
    while(wait){
      if(!client.isConnected()){
        client.connect(System.deviceID().c_str());
      }
      else{
        wait = false;
      }
    }
    client.subscribe("uuidgenResult");
    client.publish("uuidgen", id);
  }
  Serial.println("published to uuidgen");
}

void setHardware() {
  digitalWrite(BANK_0, HIGH);
  digitalWrite(BANK_1, LOW);
  digitalWrite(BANK_2, LOW);
  digitalWrite(BANK_3, LOW);
  for (int comp = 0; comp < 8; ++comp) {
    for (int i = 0; i < 8; ++i) { //ctrl line setting
      int oddPort = i + 8;
      if(components[comp][i]) {
        if(comp == 0){
          gpioControl0.digitalWrite(i, HIGH);
        }
        else if(comp == 1){
          gpioControl0.digitalWrite(oddPort, HIGH);
        }
        else if(comp == 2){
          gpioControl1.digitalWrite(i, HIGH);
        }
        else if(comp == 3){
          gpioControl1.digitalWrite(oddPort, HIGH);
        }
        else if(comp == 4){
          gpioControl2.digitalWrite(i, HIGH);
        }
        else if(comp == 5){
          gpioControl2.digitalWrite(oddPort, HIGH);
        }
        else if(comp == 6){
          gpioControl3.digitalWrite(i, HIGH);
        }
        else if(comp == 7){
          gpioControl3.digitalWrite(oddPort, HIGH);
        }
      }
      else {
        if(comp == 0){
          gpioControl0.digitalWrite(i, LOW);
        }
        else if(comp == 1){
          gpioControl0.digitalWrite(oddPort, LOW);
        }
        else if(comp == 2){
          gpioControl1.digitalWrite(i, LOW);
        }
        else if(comp == 3){
          gpioControl1.digitalWrite(oddPort, LOW);
        }
        else if(comp == 4){
          gpioControl2.digitalWrite(i, LOW);
        }
        else if(comp == 5){
          gpioControl2.digitalWrite(oddPort, LOW);
        }
        else if(comp == 6){
          gpioControl3.digitalWrite(i, LOW);
        }
        else if(comp == 7){
          gpioControl3.digitalWrite(oddPort, LOW);
        }
      }
    }
  }
  digitalWrite(BANK_0, LOW);
  digitalWrite(BANK_1, HIGH);
  digitalWrite(BANK_2, LOW);
  digitalWrite(BANK_3, LOW); //-----------BANK 1----------
  for (int comp = 8; comp < 16; ++comp) {
    for (int i = 0; i < 8; ++i) { //ctrl line setting
      int oddPort = i + 8;
      if(components[comp][i]) {
        if(comp == 8){
          gpioControl0.digitalWrite(i, HIGH);
        }
        else if(comp == 9){
          gpioControl0.digitalWrite(oddPort, HIGH);
        }
        else if(comp == 10){
          gpioControl1.digitalWrite(i, HIGH);
        }
        else if(comp == 11){
          gpioControl1.digitalWrite(oddPort, HIGH);
        }
        else if(comp == 12){
          gpioControl2.digitalWrite(i, HIGH);
        }
        else if(comp == 13){
          gpioControl2.digitalWrite(oddPort, HIGH);
        }
        else if(comp == 14){
          gpioControl3.digitalWrite(i, HIGH);
        }
        else if(comp == 15){
          gpioControl3.digitalWrite(oddPort, HIGH);
        }
      }
      else {
        if(comp == 8){
          gpioControl0.digitalWrite(i, LOW);
        }
        else if(comp == 9){
          gpioControl0.digitalWrite(oddPort, LOW);
        }
        else if(comp == 10){
          gpioControl1.digitalWrite(i, LOW);
        }
        else if(comp == 11){
          gpioControl1.digitalWrite(oddPort, LOW);
        }
        else if(comp == 12){
          gpioControl2.digitalWrite(i, LOW);
        }
        else if(comp == 13){
          gpioControl2.digitalWrite(oddPort, LOW);
        }
        else if(comp == 14){
          gpioControl3.digitalWrite(i, LOW);
        }
        else if(comp == 15){
          gpioControl3.digitalWrite(oddPort, LOW);
        }
      }
    }
  }
  int binaryBusValue [16];
  for (int i=0; i<16; i++) {
    binaryBusValue[i] = (busValue >> i) & 1;
  }
  int selectedComponent = drivingComponent - ((drivingComponent / 8) * 8);
  for(int bank = 0; bank < 4; bank++){
    setBank(bank);
    for (int i = 0; i < 16; ++i) {
      if(selectedComponent == 0 || selectedComponent == 1){
        if (binaryBusValue[i]){
          gpioData0.digitalWrite(i, HIGH);
          gpioData1.digitalWrite(i, HIGH);
          gpioData2.digitalWrite(i, HIGH);
          gpioData3.digitalWrite(i, HIGH);
        }
        else {
          gpioData0.digitalWrite(i, LOW);
          gpioData1.digitalWrite(i, LOW);
          gpioData2.digitalWrite(i, LOW);
          gpioData3.digitalWrite(i, LOW);
        }
      }
      else if(selectedComponent == 2 || selectedComponent == 3){
        if (binaryBusValue[i]){
          gpioData0.digitalWrite(i, HIGH);
          gpioData1.digitalWrite(i, HIGH);
          gpioData2.digitalWrite(i, HIGH);
          gpioData3.digitalWrite(i, HIGH);
        }
        else {
          gpioData0.digitalWrite(i, LOW);
          gpioData1.digitalWrite(i, LOW);
          gpioData2.digitalWrite(i, LOW);
          gpioData3.digitalWrite(i, LOW);
        }
      }
      else if(selectedComponent == 4 || selectedComponent == 5){
        if (binaryBusValue[i]){
          gpioData0.digitalWrite(i, HIGH);
          gpioData1.digitalWrite(i, HIGH);
          gpioData2.digitalWrite(i, HIGH);
          gpioData3.digitalWrite(i, HIGH);
        }
        else {
          gpioData0.digitalWrite(i, LOW);
          gpioData1.digitalWrite(i, LOW);
          gpioData2.digitalWrite(i, LOW);
          gpioData3.digitalWrite(i, LOW);
        }
      }
      else if(selectedComponent == 6 || selectedComponent == 7){
        if (binaryBusValue[i]){
          gpioData0.digitalWrite(i, HIGH);
          gpioData1.digitalWrite(i, HIGH);
          gpioData2.digitalWrite(i, HIGH);
          gpioData3.digitalWrite(i, HIGH);
        }
        else {
          gpioData0.digitalWrite(i, LOW);
          gpioData1.digitalWrite(i, LOW);
          gpioData2.digitalWrite(i, LOW);
          gpioData3.digitalWrite(i, LOW);
        }
      }
      else{
        if (binaryBusValue[i]){
          gpioData0.digitalWrite(i, HIGH);
          gpioData1.digitalWrite(i, HIGH);
          gpioData2.digitalWrite(i, HIGH);
          gpioData3.digitalWrite(i, HIGH);
        }
        else {
          gpioData0.digitalWrite(i, LOW);
          gpioData1.digitalWrite(i, LOW);
          gpioData2.digitalWrite(i, LOW);
          gpioData3.digitalWrite(i, LOW);
        }
      }
    }
  }
  setBank(0);
  
}

void uuidCheck() {
  Serial.println("uuid check");
  if (EEPROM.read(50) == 0xFF) {
    Serial.println("getting uuid");
    generateUUID();
  }
  else {
    char uuid[37];
    for(int i = 0; i < 36; ++i){
      uuid[i] = EEPROM.read(i + 50);
    }
    uuid[36] = '\0';
    currentUUID = (const char*)uuid;
    Serial.println("using uuid");
    setUUID(currentUUID);
  }
}

void loop() {
  Blynk.run();
  setHardware();
  if(client.isConnected()) {
    client.loop();
  }
  else{
    client.connect(System.deviceID().c_str());
    Serial.println("MQTT connection dropped");
    if(client.isConnected()){
      subscribeToUUID();
    }
  }
  /*for (int i = 0; i < 16; ++i){k
    gpioControl0.digitalWrite(i, LOW);
    Serial.print(i);
    Serial.println("HIGH");
    delay(250);
    gpioControl0.digitalWrite(i, HIGH);
    Serial.print(i);
    Serial.println("LOW");
  }*/
}